const express = require("express");
const router = express.Router();
const orderController = require("./../controllers/orderControllers")
const auth = require("./../auth");

//get all users
router.get("/", auth.verify, (req, res) => {
	orderController.getAllOrders().then(result => res.send(result))
})

//non-admin user checkout (create order)
router.post("/checkout/:productId", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	}
	orderController.createOrder(data).then(result => res.send(result))
})
 
//retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
	const data = {
		userId : auth.decode(req.headers.authorization).id,
		admin : auth.decode(req.headers.authorization).isAdmin
	}
	orderController.retrieveOrder(data).then(result => res.send(result))
})

//retrieve all orders (admin only)
router.get("/orders", auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin

	orderController.retrieveAllOrder(admin).then(result => res.send(result))
})

//stretch
//checkout orders
router.put("/checkoutmyorders", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id
	}
	orderController.checkoutMyOrders(data).then(result => res.send(result))
})

//delete order
router.post("/delete/:productId", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	}
	orderController.deleteOrder(data).then( result => res.send(result))
})

module.exports = router;
