const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers")
const auth = require("./../auth");

//check for duplicate email
router.post("/email-exists", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
})

//user registration
router.post("/register", (req, res) => {
	userController.register(req.body).then(result => res.send(result))
})

//login
router.post("/login", (req, res) => {
	userController.login(req.body).then(result => res.send(result))
})

//get all users
router.get("/", (req, res) => {
	userController.getAllUsers().then(result => res.send(result))
})

//set user as admin (admin only)
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		userId: req.params.userId
	}
	userController.setAsAdmin(data).then(result => res.send(result))
})

//stretch
////unset user as admin (admin only)
router.put("/:userId/unsetAsAdmin", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		userId: req.params.userId
	}
	userController.unsetAsAdmin(data).then(result => res.send(result))
})

//get user details
router.get("/details", auth.verify, (req, res) => {
	
	let userData = auth.decode(req.headers.authorization)
	userController.getProfile(userData).then(result => res.send(result))
})

module.exports = router;