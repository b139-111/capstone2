const express = require("express");
const router = express.Router();
const productController = require("./../controllers/productControllers")
const auth = require("./../auth");

//retrieve all products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(result => res.send(result))
})

//retrieve all active products
router.get("/", (req, res) => {
	productController.getActiveProducts().then(result => res.send(result))
})

//retrieve single product
router.get("/:productId", (req, res) => {
	productController.getSpecificProduct(req.params.productId).then(result => res.send(result))
})

//create product (admin only)
router.post("/", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body
	}
	productController.createProduct(data).then(result => res.send(result))
})

//update product information (admin only)
router.put("/:productId/productId", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.params.productId,
		reqBody: req.body
	}
	productController.updateProduct(data).then(result => res.send(result))
})

//archive product (admin only)
router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId
	}
	productController.archiveProduct(data).then(result => res.send(result))
})

//stretch
////unarchive product (admin only)
router.put("/:productId/unarchive", auth.verify, (req, res) => {
	const data = {
		admin : auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId
	}
	productController.unarchiveProduct(data).then(result => res.send(result))
})

//delete product
router.delete("/:productId/delete", auth.verify, (req, res) => {

	productController.deleteProduct(req.params.productId).then( result => res.send(result))
})
module.exports = router;