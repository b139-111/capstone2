const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 4000;
const cors = require("cors");

//Routes module
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");


//Mongoose
mongoose.connect('mongodb+srv://Keanu_Orig:J3^bmsqa@batch139.1lth5.mongodb.net/Capstone-2?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors())

app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);

app.listen(PORT, () => console.log(`Server is running`))

//user registration
//user authentication
//set user as admin (admin only)
//retrieve all active products
//retrieve single product
//create product (admin only)
//update product information (admin only)
//archive product (admin only)
//non-admin user checkout (create order)
//retrieve authenticated user's orders
//retrieve all orders (admin only)