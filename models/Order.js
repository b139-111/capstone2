
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	totalPrice: {
		type: Number,
	},
	totalAmount: {
		type: Number,
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	productId: {
		type: Array,
		required: [true, "Product ID is required"]
	},
	isCheckout: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model("Order", orderSchema);