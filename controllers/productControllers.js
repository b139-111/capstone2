const Product = require("./../models/Product");

//retrieve all products
module.exports.getAllProducts = () => {

	return Product.find().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

//retrieve all active products
module.exports.getActiveProducts = () => {

	return Product.find({isActive: true}).then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

//retrieve single product
module.exports.getSpecificProduct = (params) => {
	return Product.findById(params).then((result, error) => {
		if(result == null) {
			return 'Product not existing'
		} else {

			if(result){
				return result
			} else{
				return false
			}
		}
	})
}

//create product (admin only)
module.exports.createProduct = async (data) => {
	const {admin, reqBody} = data;
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	if(admin) {
		return newProduct.save().then( (result, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	} else {
		return `Access Denied!`
	}

}

//update product information (admin only)
module.exports.updateProduct = (data) => {
	const {admin, productId, reqBody} = data
	const {name, description, price} = reqBody
	let newProduct = {
		name: name,
		description: description,
		price: price
	}

	return Product.findByIdAndUpdate(productId, newProduct, {new: true}).then( (result, error) => {
		if(result == null) {
			return 'Product does not exist!'
		} else if(admin == false) {
			return 'Access Denied!'
		} else {
			return result
		}
	})
}





//archive product (admin only)
module.exports.archiveProduct = (data) => {
	const {admin, productId} = data
	let updatedIsActive = {
		isActive: false
	}
	return Product.findByIdAndUpdate(productId, updatedIsActive, {new: true}).then( (result, error) => {
		if(result == null) {
			return 'Product does not exist!'
		} else if(admin == false) {
			return 'Access Denied!'
		} else {
			return result
		}
	})
}

//stretch
////unarchive product (admin only)
module.exports.unarchiveProduct = (data) => {
	const {admin, productId} = data
	let updatedIsActive = {
		isActive: true
	}
	return Product.findByIdAndUpdate(productId, updatedIsActive, {new: true}).then( (result, error) => {
		if(result == null) {
			return 'Product does not exist!'
		} else if(admin == false) {
			return 'Access Denied!'
		} else {
			return result
		}
	})
}

//delete product
module.exports.deleteProduct = (params) => {

	return Product.findByIdAndDelete(params).then( result => {
		if(result == null) {
			return 'Product not existing'
		} else {
			if(result){
				return true
			} else{
				return false
			}
		}
	})
}