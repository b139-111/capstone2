const Order = require("./../models/Order");
const User = require("./../models/User");
const Product = require("./../models/Product");

//get all orders
module.exports.getAllOrders = () => {

	return Order.find().then( (res, err) => {
		if(err){
			return false
		} else {
			return res
		}
	})
}

//non-admin user checkout (create order)
module.exports.createOrder = (data) => {
	const {admin, userId, productId} = data

	return Order.findOne({userId: userId}).then((result, error) => {
		if (result == null && admin == false){
			return Product.findById(productId).then((res, err) => {
				if (err) {
					return err
				} else {
					let newOrder = new Order({
						userId: userId,
						productId: productId,
						totalAmount: 1,
						totalPrice: res.price
					})
					return newOrder.save().then( (res, err) => {
						if(err){
							return false
						} else {
							return res
						}
					})
				}
			})
		} else if(result && admin == false && result.isCheckout == false){
			return Product.findById(productId).then((res, err) => {
				if (err) {
					return err
				} else {
					result.totalAmount += 1;
					result.productId.push(productId)	
					result.totalPrice += res.price
					return result.save().then( (res, err) => {
						if(err){
							return false
						} else {
							return res
						}
					})
				}
			})
		} else {
			return `Unable to create order!`
		}
	})
}

//retrieve authenticated user's orders
module.exports.retrieveOrder = async(data) => {
	let {userId, admin} = data
	if(admin == false) {
		return Order.findOne({userId: userId}).then( (res, err) => {
			if(err){
				return false
			} else {
				return res
			}
		})
	}
	else {
		return `Access Denied!`
	}
}

//retrieve all orders (admin only)
module.exports.retrieveAllOrder = async(admin) => {
	if(admin) {
		return Order.find({isCheckout: true}).then( (res, err) => {
			if(err){
				return false
			} else {
				return res
			}
		})
	}
	else {
		return `Access Denied!`
	}
}

//stretch
//checkout orders
module.exports.checkoutMyOrders = async (data) => {
	const {admin, userId} = data
	let updatedIsCheckout = {
		isCheckout: true
	}
	if (admin == false) {
		return Order.findOne({userId: userId}).then( (res, err) => {
			if (err){
				return err
			} else if (res.productId.length > 0){
				return Order.findOneAndUpdate({userId: userId}, updatedIsCheckout, {new: true}).then( (result, error) => {
					if(result == null) {
						return false
					} else {
						return `Checked out!`
					}
				})
			} else {
				return `No item found in cart!`
			}
		})
	} else {
		return false
	}
}

//delete order
module.exports.deleteOrder = (data) => {
	const {admin, userId, productId} = data

	return Order.findOne({userId: userId}).then((result, error) => {
		if (result && admin == false && result.isCheckout == false){
			return Product.findById(productId).then((res, err) => {
				if (err) {
					return false
				} else {
					for (let i = 0; i < result.productId.length; i++) {
				        if (result.productId[i] == productId) {
			         		let index = result.productId.indexOf(productId);
			         		result.productId.splice(index,1);
			         		result.totalAmount -= 1;	
			         		result.totalPrice -= res.price
			         		return result.save().then( (result, error) => {
			         			if(error){
			         				return false
			         			} else {
			         				return true
			         			}
			         		})
			         	}
					}
					
				}
			})
		} else {
			return `Unable to delete order!`
		}
	})
}

/*//non-admin user checkout (create order)
module.exports.createOrder = (data) => {
	const {userId, productId} = data

	return Order.findOne({userId: userId}).then((result, error) => {
		if (result == null){
			return Product.findById(productId).then((res, err) => {
				if (err) {
					return err
				} else {
					let newOrder = new Order({
						userId: userId,
						productId: productId,
						totalAmount: 1
					})
					return newOrder.save().then( (res, err) => {
						if(err){
							return false
						} else {
							return true
						}
					})
				}
			})
		} else if(result){
			return Product.findById(productId).then((res, err) => {
				console.log(result)
				console.log(res)
				if (err) {
					return err
				} else { // confirm if for single or multiple product order
					let LoopProduct = result.productId;
					for (let i = 0; i < LoopProduct.length; i++) {
						console.log(LoopProduct)
						if (LoopProduct[i] == productId) {
							result.totalAmount += 1;
							return result.save().then( (result, error) => {
								if(error){
									return false
								} else {
									return true
								}
							})
						} else if (LoopProduct[i] !== productId){
							console.log(`else`)
							result.productId.push(productId)	
							return result.save().then( (result, error) => {
								if(error){
									return false
								} else {
									return true
								}
							})
						}
					}
					
				}
			})
		} else {
			return false
		}
	})
}*/
