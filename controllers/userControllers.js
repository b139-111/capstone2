const User = require("./../models/User");
const bcrypt = require("bcrypt");
const auth = require("./../auth");
const Order = require("./../models/Order");
const Product = require("./../models/Product");

//check for duplicate email
module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody
	return User.findOne({email: email}).then( (res, err) => {
		if(res != null) {
			return 'Email already exists!'
		} else {
			if(res == null){
				return true
			} else{
				return error
			}
		}
	})
}

//user registration
module.exports.register = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (res, err) => {
		if(res){
			return true
		} else {
			return false
		}
	})
}

//login
module.exports.login = (reqBody) => {
	const {email, password} = reqBody;

	return User.findOne({email: email}).then( (res, err) => {
		if(res == null) {
			return false
		} else {
			let isPasswordCorrect = bcrypt.compareSync(password, res.password) //user, server
			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(res)}
			} else{
				return err
			}
		}
	})
}

//get all users
module.exports.getAllUsers = () => {

	return User.find().then( (res, err) => {
		if(err){
			return false
		} else {
			return res
		}
	})
}

//set user as admin (admin only)
module.exports.setAsAdmin = (data) => {
	let updatedIsAdmin = {isAdmin: true}

	return User.findByIdAndUpdate(data.userId, updatedIsAdmin, {new: true}).then( result => {
		if(result == null) {
			return 'User does not exist!'
		} else if(data.admin == false) {
			return 'Access Denied!'
		} else {
			return result
		}
	})
}

//stretch
////unset user as admin (admin only)
module.exports.unsetAsAdmin = (data) => {
	let updatedIsAdmin = {isAdmin: false}

	return User.findByIdAndUpdate(data.userId, updatedIsAdmin, {new: true}).then( result => {
		if(result == null) {
			return 'User does not exist!'
		} else if(data.admin == false) {
			return 'Access Denied!'
		} else {
			return result
		}
	})
}

module.exports.getProfile = (data) => {
	const {id} = data

	return User.findById(id).then((result, error) => {

		if(result != null) {
			result.password = ""
			return result
			} else {
				return false
			}
	})
}